FROM tiangolo/uvicorn-gunicorn-fastapi:python3.8-slim
#FROM tiangolo/uvicorn-gunicorn-fastapi:latest

RUN apt-get update && apt-get install -y apt-utils
RUN apt-get update && apt-get install -y \
    build-essential \
#    python3-dev \
    libpq-dev \
    && rm -rf /var/lib/apt/lists/*

COPY requirements.txt requirements.txt
RUN /usr/local/bin/python -m pip install --upgrade pip
RUN pip install -r requirements.txt

COPY ./src/app /app

RUN mkdir -p /data/tableData
RUN mkdir -p /data/zipData