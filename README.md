This project is deprecated in favour of <https://gitlab.com/calincs/infrastructure/geonames-api>

# Geonames API (LINCS edition)

## Context

Linked data preparation includes the important task of enriching data through reconciliation. During reconciliation, entities (persons, places, or things) are extracted from data, often in string form (i.e. in linked data terms, a "literal"). The reconciliation script sends requests to a web service, called an Application Programming Interface (API), which is hosted by a linked data authority (i.e. VIAF, Getty, LOC). The API receives the string and returns a set of matching records containing canonical URIs, one of which may be used to provide a standardized linked data representation of the given person, place, or thing. 

Geographical context plays an enormous analytical role in humanities research and, therefore, reconciliation of geographical entities plays an enormous role in the LINCS data pipeline. One of the authorities providing LINCS with standardized geographical entities is Geonames. The Geonames API provides a place name lookup service; however, there is a limit on API requests: 20,000 credits daily limit per application, the hourly limit is 1000 credits. Twenty thousand requests can add up quickly in a day, and a thousand can easily be reached in an hour. The LINCS team identified this as a potential barrier. The solution was simple: download the data directly from geonames and host our own API for internal use.

Try our [hosted instance](http://geonames.lincsproject.ca/docs).

## Description

The LINCS project has developed this locally hosted Geonames API for internal use cases. The service should replace current uses for the external Geonames web service, but it is not intended to extend to services that require complex geographical interpolation or highly granular data (i.e. nearest POI, address/postal code lookup). The service provides a Postgresql backend, a web API, and automated data import.

## Technical specs

### PostgreSQL

The PostgreSQL object-relational database system provides reliability and data integrity. It has been implemented as backend storage using the latest image from [Docker](https://hub.docker.com/_/postgres).

### uvicorn-gunicorn-fastapi-docker

The base image for this project: a docker image with Uvicorn managed by Gunicorn for high-performance FastAPI web applications in Python 3.6 and above with performance auto-tuning.

GitHub repo: https://github.com/tiangolo/uvicorn-gunicorn-fastapi-docker

Docker Hub image: https://hub.docker.com/r/tiangolo/uvicorn-gunicorn-fastapi/

FastAPI is a modern, fast (high-performance), web framework for building APIs with Python 3.6+.

The key features are:

* Fast: Very high performance, on par with NodeJS and Go (thanks to Starlette and Pydantic).
* Fast to code: Increase the speed to develop features by about 300% to 500%.
* Less bugs: Reduce about 40% of human (developer) induced errors.
* Intuitive: Great editor support. Completion everywhere. Less time debugging.
* Easy: Designed to be easy to use and learn. Less time reading docs.
* Short: Minimize code duplication. Multiple features from each parameter declaration. Less bugs.
* Robust: Get production-ready code. With automatic interactive documentation.
* Standards-based: Based on (and fully compatible with) the open standards for APIs: OpenAPI (previously known as Swagger) and JSON Schema.

## How to Use

### Docker-compose

When running locally, the database receives credentials from the file `src/app/.env`, which you have to create yourself:

```bash
POSTGRES_USER=geoUser
PGPASSWORD=password
POSTGRES_DB=geo_db
POSTGRES_HOST=geonames-api_db_1
POSTGRES_PORT=5432
```

For local installation. Use Docker Compose. You will have to build the image first:

`docker-compose up --build -d'

Deployment on gitlab is handled by gitlab auto ci. See `.gitlab-ci.yaml`.

See api documentation at http://geonames.lincsproject.ca/docs

### Import

Importing data into the database is handled automatically. The script (`import.py`) is initiated by the api container. It looks for a geonames table in the database. If it does not exist, the import script will download the files and build the database. The process takes a couple of hours. No actions are required.

### Development mode

The `/app` folder is mounted as a volume, so changes to the codebase will be reflected in the container. To enable hot-reload, run FASTAPI using uvicorn instead of gunicorn. In docker-compose.yaml, remove commenting: `["uvicorn", "main:app", "--host", "0.0.0.0", "--debug", "--port", "80", "--reload-dir", "/app"]`.

## See also

* [Geonames-MySQL-Import](https://github.com/codigofuerte/GeoNames-MySQL-DataImport) provided the initial inspiration for importing geonames dump files into a relational database.

* [Pelias](http://pelias.io) is the next step in creating a fully-functional geo-lookup service. It is possible to create a full-planet model that, while impressive, is massively resource intensive.

## To do

* Implement a test suite
* Test using Postman
* Create a landing page
* Consider authentication