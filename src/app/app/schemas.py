from typing import List, Optional
from pydantic import BaseModel


class GeonamesBase(BaseModel):
    name: str
    alternatenames: Optional[str] = None
    latitude: float
    longitude: float
    country: str
    country_name: str
    population: int
    fclass: str
    feature_name: str
    feature_desc: str
    admin1_desc: str
    admin2_desc: str
    uri: str
    rank: int

class Geonames(GeonamesBase):
    geonameid: int

    class Config:
        orm_mode = True

class CountryInfo(BaseModel):
    iso_alpha2: str
    country_name: str