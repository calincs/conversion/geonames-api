from sqlalchemy.orm import Session
from . import models
import pandas as pd
from fastapi.encoders import jsonable_encoder
from fastapi.responses import JSONResponse
import json

default_sort = ["A","P","H","V","L","S","T","R","U"]

def sort_dataframe(result, custom_sort, limit, pop): 
    #convert query result to a dataframe
    result = pd.DataFrame(list(result))           
    # Create the dictionary that defines the order for sorting
    sorterIndex = dict(zip(custom_sort, range(len(custom_sort))))
    # Generate a rank column that will be used to sort the dataframe numerically based on fclass relative to the list parameter
    result['custom_sort'] = result['fclass'].map(sorterIndex)
    #  lexicographic sort ascending, population descending
    if pop:
        result.sort_values(['custom_sort', 'population'], ascending = [True, False], inplace = True)
    else:
        result.sort_values(['custom_sort'], ascending = [True], inplace = True)
    result.drop('custom_sort', 1, inplace = True)
    # apply the limit after sorting
    result = result.head(limit)
    result = json.loads(result.to_json(orient='records', force_ascii=False))
    return result

def get_country(db: Session):
    return JSONResponse(jsonable_encoder(db.query(models.CountryInfo).filter().all()))

def get_geoname_by_id(db: Session, geonameid: int):
    return db.query(models.Geonames).get(geonameid)

def get_geoname_by_name(db: Session, name: str, country_filter: str, sort_by_population: bool, custom_sort: list, limit: int):
    if country_filter and sort_by_population:
        #filter by country, sort by pop and by feature
        if custom_sort == default_sort:
            return db.query(models.Geonames).filter(models.Geonames.country_name.ilike(country_filter), models.Geonames.name.ilike(name)).order_by(models.Geonames.rank.asc(), models.Geonames.population.desc()).limit(limit).all()
        else:
            result = sort_dataframe(jsonable_encoder(db.query(models.Geonames).filter(models.Geonames.country_name.ilike(country_filter), models.Geonames.name.ilike(name)).all()), custom_sort=custom_sort, limit=limit, pop=True)
            return JSONResponse(result)
    elif country_filter and not sort_by_population:
        #filter by country, sort by feature
        if custom_sort == default_sort:
            return db.query(models.Geonames).filter(models.Geonames.country_name.ilike(country_filter), models.Geonames.name.ilike(name)).order_by(models.Geonames.rank.asc()).limit(limit).all()
        else:
            result = sort_dataframe(jsonable_encoder(db.query(models.Geonames).filter(models.Geonames.country_name.ilike(country_filter), models.Geonames.name.ilike(name)).all()), custom_sort=custom_sort, limit=limit, pop=False)        
            return JSONResponse(result)
    elif sort_by_population and not country_filter:
        #sort by pop and feature 
        if custom_sort == default_sort:
            return db.query(models.Geonames).filter(models.Geonames.name.ilike(name)).order_by(models.Geonames.rank.asc(), models.Geonames.population.desc()).limit(limit).all()
        else:
            result = sort_dataframe(jsonable_encoder(db.query(models.Geonames).filter(models.Geonames.name.ilike(name)).all()), custom_sort=custom_sort, limit=limit, pop=True)    
            return JSONResponse(result)
    elif not sort_by_population and not country_filter:
        #sort by feature only
        if custom_sort == default_sort:
            return db.query(models.Geonames).filter(models.Geonames.name.ilike(name)).order_by(models.Geonames.rank.asc()).limit(limit).all()
        else:
            result = sort_dataframe(jsonable_encoder(db.query(models.Geonames).filter(models.Geonames.name.ilike(name)).all()), custom_sort=custom_sort, limit=limit, pop=False)
            return JSONResponse(result)
def get_geoname_by_alternate_name(db: Session, alternate_name: str, country_filter: str, sort_by_population: bool, custom_sort: list, limit: int):
    if country_filter and sort_by_population:
        #filter by country, sort by pop and by feature
        if custom_sort == default_sort:
            return db.query(models.Geonames).filter(models.Geonames.alternatenames.ilike(alternate_name), models.Geonames.country_name.ilike(country_filter)).order_by(models.Geonames.rank.asc(), models.Geonames.population.desc()).limit(limit).all()
        else:
            result = sort_dataframe(jsonable_encoder(db.query(models.Geonames).filter(models.Geonames.alternatenames.ilike(alternate_name), models.Geonames.country_name.ilike(country_filter)).all()), custom_sort=custom_sort, limit=limit, pop=True)
            return JSONResponse(result)

    elif country_filter and not sort_by_population:
        #filter by country, sort by feature
        if custom_sort == default_sort:
            return db.query(models.Geonames).filter(models.Geonames.alternatenames.ilike(alternate_name), models.Geonames.country_name.ilike(country_filter)).order_by(models.Geonames.rank.asc()).limit(limit).all()
        else:
            result = sort_dataframe(jsonable_encoder(db.query(models.Geonames).filter(models.Geonames.alternatenames.ilike(alternate_name), models.Geonames.country_name.ilike(country_filter)).all()), custom_sort=custom_sort, limit=limit, pop=False)
            return JSONResponse(result)
    elif sort_by_population and not country_filter:
        #sort by pop and feature
        if custom_sort == default_sort:
            return db.query(models.Geonames).filter(models.Geonames.alternatenames.ilike(alternate_name)).order_by(models.Geonames.rank.asc(), models.Geonames.population.desc()).limit(limit).all()
        else:
            result = sort_dataframe(jsonable_encoder(db.query(models.Geonames).filter(models.Geonames.alternatenames.ilike(alternate_name)).all()), custom_sort=custom_sort, limit=limit, pop=True)
            return JSONResponse(result)
        
    elif not country_filter and not sort_by_population:
        #sort by feature only
        if custom_sort == default_sort:
            return db.query(models.Geonames).filter(models.Geonames.alternatenames.ilike(alternate_name)).order_by(models.Geonames.rank.asc()).limit(limit).all()
        else:
            result = sort_dataframe(jsonable_encoder(db.query(models.Geonames).filter(models.Geonames.alternatenames.ilike(alternate_name)).all()), custom_sort=custom_sort, limit=limit, pop=False)
            return JSONResponse(result)

def get_geoname_by_exact_name(db: Session, name: str, country_filter: str, sort_by_population: bool, custom_sort: list, limit: int):
    if country_filter and sort_by_population:
        #filter by country, sort by pop and by feature
        if custom_sort == default_sort:
            return db.query(models.Geonames).filter(models.Geonames.name == name, models.Geonames.country_name.ilike(country_filter)).order_by(models.Geonames.rank.asc(), models.Geonames.population.desc()).limit(limit).all()
        else:
            result = sort_dataframe(jsonable_encoder(db.query(models.Geonames).filter(models.Geonames.name == name, models.Geonames.country_name.ilike(country_filter)).all()), custom_sort=custom_sort, limit=limit, pop=True)
            return JSONResponse(result)
    elif country_filter and not sort_by_population:
        #filter by country, sort by feature
        if custom_sort == default_sort:
            return db.query(models.Geonames).filter(models.Geonames.name == name, models.Geonames.country_name.ilike(country_filter)).order_by(models.Geonames.rank.asc()).limit(limit).all()
        else:
            result = sort_dataframe(jsonable_encoder(db.query(models.Geonames).filter(models.Geonames.name == name, models.Geonames.country_name.ilike(country_filter)).all()), custom_sort=custom_sort, limit=limit, pop=False)
            return JSONResponse(result)
    elif sort_by_population and not country_filter:
        #sort by pop and feature 
        if custom_sort == default_sort:
            return db.query(models.Geonames).filter(models.Geonames.name == name).order_by(models.Geonames.rank.asc(), models.Geonames.population.desc()).limit(limit).all()
        else:
            result = sort_dataframe(jsonable_encoder(db.query(models.Geonames).filter(models.Geonames.name == name).all()), custom_sort=custom_sort, limit=limit, pop=True)
            return JSONResponse(result)
    elif not country_filter and not sort_by_population:
        #sort by feature only
        if custom_sort == default_sort:
            return db.query(models.Geonames).filter(models.Geonames.name == name).order_by(models.Geonames.rank.asc()).limit(limit).all()
        else:
            result = sort_dataframe(jsonable_encoder(db.query(models.Geonames).filter(models.Geonames.name == name).all()), custom_sort=custom_sort, limit=limit, pop=False)
            return JSONResponse(result)
