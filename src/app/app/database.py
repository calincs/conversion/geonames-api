from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import os

SQLALCHEMY_DATABASE_URL = os.getenv(
    "DATABASE_URL",
    "postgresql://{}:{}@{}:{}/{}".format(os.getenv("POSTGRES_USER", None), os.getenv("PGPASSWORD", None), os.getenv("POSTGRES_HOST", None), os.getenv("POSTGRES_PORT", None), os.getenv("POSTGRES_DB", None))
).replace("postgres:", "postgresql:")

def dbConnect():
    print('Connecting to {}'.format(SQLALCHEMY_DATABASE_URL))
    try:
        db = create_engine(SQLALCHEMY_DATABASE_URL, echo=True)
        print('Connection created')
        return db
    except Exception as e:
        print('Failed to connect: {}',format(e))

engine = dbConnect()
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
Base = declarative_base()
