from sqlalchemy import Column, Integer, Numeric, String, Unicode
from .database import Base
from sqlalchemy.ext.hybrid import hybrid_property

class Geonames(Base):
    __tablename__ = "geonames"
    geonameid = Column(Integer, primary_key=True, index=True)
    name = Column(Unicode(200))
    alternatenames = Column(Unicode(4000))
    latitude = Column(Numeric(precision=8, asdecimal=False, decimal_return_scale=None))
    longitude = Column(Numeric(precision=8, asdecimal=False, decimal_return_scale=None))
    country = Column(Unicode(2))
    country_name = Column(Unicode(200))
    population = Column(Integer, index=True)
    fclass = Column(Unicode(1))
    feature_name = Column(Unicode(200))
    feature_desc = Column(Unicode())
    admin1_desc = Column(Unicode())
    admin2_desc = Column(Unicode())
    rank = Column(Integer)
    
    def __init__(self, geonameid):
        self.geonameid = geonameid

    @hybrid_property
    def uri(self):
        return "http://geonames.org/{}".format(self.geonameid)

class CountryInfo(Base):
    __tablename__ = "country_info"
    iso_alpha2 = Column(Unicode(2), primary_key=True, index=True)
    country_name = Column(Unicode(200))