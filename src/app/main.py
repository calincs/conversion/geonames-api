from typing import List, Optional
import uvicorn
from sqlalchemy.orm import Session
from fastapi import Depends, FastAPI, HTTPException, Query
from fastapi.responses import JSONResponse, HTMLResponse
from app import models, schemas, crud
from app.database import engine, SessionLocal

models.Base.metadata.create_all(bind=engine)

app = FastAPI(
    title="Geonames API - LINCS Edition",
    description="A very simple API for fetching geoname records.",
    version="0.1",
)

# Dependency
def get_db():
    db = None
    try:
        db = SessionLocal()
        yield db
    finally:
        db.close()

@app.get("/")
def get_hello():
    html_content = """
    <html>
        <head>
            <title>LINCS GeoNames Service</title>
        </head>
        <body>
            <h1>LINCS GeoNames Service</h1>
        </body>
    </html>
    """
    return HTMLResponse(content=html_content, status_code=200)

@app.get("/geonames/alternate_names/{alternate_name}", response_model=List[schemas.Geonames])
def geoname_by_approximate_alternate_names(alternate_name: str = Query(None, Title="Search Alternate Placenames (approximate matches)", Description="Searches for partial matches against alternative names. Case insensitive."), country_filter: Optional[str] = None, sort_by_population: Optional[bool] = None, custom_sort: Optional[List[str]] = Query(["A","P","H","V","L","S","T","R","U"]), limit: Optional[int] = 20, db: Session = Depends(get_db)):
    db_geoname = crud.get_geoname_by_alternate_name(db, alternate_name="%{}%".format(alternate_name), country_filter=country_filter, sort_by_population=sort_by_population, custom_sort=custom_sort, limit=limit)
    if db_geoname is None:
        raise HTTPException(status_code=404, detail="Geoname not found")
    return db_geoname

@app.get("/geonames/approx_name/{name}", response_model=List[schemas.Geonames])
def geoname_by_approximate_name(name: str = Query(None, Title="Search Placenames (approximate matches)", Description="Searches for partial matches against official place names. Case insensitive."), country_filter: Optional[str] = None, sort_by_population: Optional[bool] = None, custom_sort: Optional[List[str]] = Query(["A","P","H","V","L","S","T","R","U"]), limit: Optional[int] = 20, db: Session = Depends(get_db)):
    db_geoname = crud.get_geoname_by_name(db, name="%{}%".format(name), country_filter=country_filter, sort_by_population=sort_by_population, custom_sort=custom_sort, limit=limit)
    if db_geoname is None:
        raise HTTPException(status_code=404, detail="Geoname not found")
    return db_geoname

@app.get("/geonames/exact_name/{name}", response_model=List[schemas.Geonames])
def geoname_by_exact_name(name: str = Query(None, Title="Search Placenames (exact matches)", Description="Searches for exact matches against official place names. Case insensitive."), country_filter: Optional[str] = None, sort_by_population: Optional[bool] = None, custom_sort: Optional[List[str]] = Query(["A","P","H","V","L","S","T","R","U"]), limit: Optional[int] = 20, db: Session = Depends(get_db)):
    db_geoname = crud.get_geoname_by_exact_name(db, name=name, country_filter=country_filter, sort_by_population=sort_by_population, custom_sort=custom_sort, limit=limit)
    if db_geoname is None:
        raise HTTPException(status_code=404, detail="Geoname not found")
    return db_geoname

@app.get("/geonames/id/{geonameid}", response_model=schemas.Geonames)
def geoname_by_id(geonameid = int, db: Session = Depends(get_db)):
    db_geoname = crud.get_geoname_by_id(db, geonameid=geonameid)
    if db_geoname is None:
        raise HTTPException(status_code=404, detail="Geoname not found")
    return db_geoname

@app.get("/geonames/query", response_model=schemas.Geonames)
def id_by_parameter(geonameid: int = 1, db: Session = Depends(get_db)):
    db_geoname = crud.get_geoname_by_id(db, geonameid=geonameid)
    if db_geoname is None:
        raise HTTPException(status_code=200, detail="Geoname not found")
    return db_geoname

@app.get("/countryInfo/", response_model=schemas.CountryInfo)
def get_country_info(db: Session = Depends(get_db)):
    db_countryname = crud.get_country(db)
    if db_countryname is None:
        raise HTTPException(status_code=404, detail="Country not found")
    return db_countryname



@app.get("/default_sort/")
def get_default_sort():
    return JSONResponse(
        {
            "default sort order": {
                "A": "country, state, region",
                "P": "city, village",
                "H": "stream, lake",
                "V": "forest,heath",
                "L": "parks, area",
                "S": "spot, building, farm",
                "T": "mountain, hill, rock",
                "R": "road, railroad",
                "U": "undersea"
            }
        }
    )

if __name__ == "__main__":
    uvicorn.run(app, host="127.0.0.1", port=8081)
