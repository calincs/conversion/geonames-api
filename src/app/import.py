import requests
import zipfile
import os
import pandas as pd
from app.database import engine as db

# download endpoint, file names, and data locations
config = {
    "txt_filenames": ["admin1CodesASCII.txt", "admin2Codes.txt", "featureCodes_en.txt", "countryInfo.txt"],
    "geonamesGeneralDataRepo": "http://download.geonames.org/export/dump/",
    "zip_folder": "/data/zipData/",
    "data_folder": "/data/tableData/"
}

def dbCheck():
    """
    Checks the database for a geonames table with more than one row. 
    Returns true if exists, false if not.
    """

    print('Checking for existing data...')

    with db.connect() as con:
        try:
            resultproxy = con.execute('SELECT COUNT(geonameid) as count FROM geonames')
            
            if [{column: value for column, value in rowproxy.items()} for rowproxy in resultproxy][0]['count'] > 1:
                print('Data already exists. Terminating import.')
                return True
            else:
                print('Data not found. Proceeding with import.')
                return False
        except:
            print('Data not found. Proceeding with import.')
            return False    

def downloadData():

    """
    Checks if files are downloaded. Downloads them if not.
    Returns nothing.
    """

    if not os.path.exists(config['data_folder']):
        os.makedirs(config['data_folder'])
    if not os.path.exists(config['zip_folder']):
        os.makedirs(config['zip_folder'])

    # download allCountries.zip
    if not 'allCountries.txt' in os.listdir(config['data_folder']):
        print("allCountries.zip downloading")
        url = "{}allCountries.zip".format(config['geonamesGeneralDataRepo'])
        r = requests.get(url, allow_redirects=True)
        open("{}{}".format(config['zip_folder'], 'allCountries.zip'), "wb").write(r.content)
        with zipfile.ZipFile("{}{}".format(config['zip_folder'], 'allCountries.zip'), 'r') as zip_ref:
            zip_ref.extractall(config['data_folder'])
        print("allCountries.txt saved.")
    else:
        print("allCountries.txt already satisfied.")

    # download supporting data
    for filename in config['txt_filenames']:
        data_path = "{}{}".format(config['data_folder'], filename)
        if not filename in os.listdir(config['data_folder']):
            print("{} downloading".format(filename))
            url = "{}{}".format(config['geonamesGeneralDataRepo'], filename)
            r = requests.get(url, allow_redirects=True)
            open(data_path, "wb").write(r.content)
            print("{} saved.".format(filename))
        else:
            print("{} already satisfied.".format(filename))

def transformData():
    """
    Converts csv files into pandas dataframe. Joins critical data to the main geonames table.
    Does the same conversion for alternate_names table, which we are including for future development.
    Returns two pandas dataframes in a dictionary.
    """
    print('Transforming data...')
    
    # check if sample size is set in environment. Sample size assigned to nrows.
    if os.getenv("SAMPLE_SIZE", None):
        nrows = int(os.getenv("SAMPLE_SIZE"))
    else:
        nrows = None
    
    # load the main geonames table
    data = pd.read_csv(
        "/data/tableData/allCountries.txt", 
        sep="\t", 
        header=None, 
        names = ["geonameid", "name", "asciiname", "alternatenames", "latitude", "longitude", "fclass", "fcode", "country", "cc2", "admin1", "admin2", "admin3_desc", "admin4_desc", "population", "elevation", "gtopo30", "timezone", "moddate"],
        usecols = ["geonameid", "name","alternatenames", "latitude", "longitude","fclass", "fcode", "country",  "admin1", "admin2", "admin3_desc", "admin4_desc"],
        dtype={"admin1": "object", "admin2": "object", "admin3_desc": "object", "admin4_desc": "object"},
        nrows=nrows
        )

    # prepare the codes for joining
    data["admin1_code"] = data['country'] + "." + data["admin1"]
    data["admin2_code"] = data['admin1_code'] + "." + data["admin2"]
    data['feature'] = data['fclass'] + "." + data['fcode']
    
    # strip off artifacts
    data = data[["geonameid", "name", "alternatenames", "latitude", "longitude", "fclass", "feature", "country",  "admin1_code", "admin2_code", "admin3_desc", "admin4_desc"]]

    # read-in administrative data and merge it to the main table
    admin_1 = pd.read_csv("/data/tableData/admin1CodesASCII.txt", sep="\t", header=None, names = ['code', 'admin1_desc', 'asciiname', 'geonameid'], usecols=['code', 'admin1_desc'])
    data = data.merge(admin_1, how='left', left_on="admin1_code", right_on="code")
    del admin_1
    
    admin_2 = pd.read_csv("/data/tableData/admin2Codes.txt", sep="\t", header=None, names = ['code', 'admin2_desc', 'asciiname', 'geonameid'], usecols=['code', 'admin2_desc'])
    data = data.merge(admin_2, how='left', left_on='admin2_code', right_on="code")
    del admin_2
    
    # read in country info to pull country name from, and merge it
    country_info = pd.read_csv(
        "/data/tableData/countryInfo.txt", 
        sep="\t", 
        skiprows=50, 
        header=None, 
        names=["iso_alpha2", "iso_alpha3", "iso_numeric", "fips_code", "country_name", "capital", "areaInSqKm", "population", "continent", "tld", "currency", "currencyName", "phone", "postalCodeFormat", "postalCodeRegex", "languages", "geonameid", "neighbours", "equivalentFipsCode"],
        usecols=["iso_alpha2", "country_name", "population"]
        )
    data = data.merge(country_info, how="left", left_on='country', right_on='iso_alpha2')
    # some rows in the data do not match on iso_alpha2. So be it. We have set those rows' population to zero.
    data['population'] = data['population'].fillna(0).astype('int64')
    country_info.drop("population", 1, inplace=True)
    
    # read-in feature codes and merge it. Feature codes give us the description of the feature.
    feature_code = pd.read_csv("/data/tableData/featureCodes_en.txt", sep="\t", header=None, names=["code", "feature_name", "feature_desc"])
    data = data.merge(feature_code, how="left", left_on="feature", right_on="code")
    del feature_code

    data = data[['geonameid', 'name', 'alternatenames', 'country', 'country_name', 'population', 'fclass', 'feature_name', 'feature_desc', 'admin1_desc', 'admin2_desc', 'admin3_desc', 'admin4_desc', 'latitude', 'longitude']]
    data = data.fillna("")
    data['rank'] = data['fclass'].map(lambda x: ["A","P","H","V","L","S","T","R","U",""].index(x))

    print('Success.')

    return data, country_info


def loadData(data, country_info):
    """ 
    Passes pandas dataframes (geonames table, alternate names table) to the database.
    Returns nothing.
    """
    print('Loading data into database')
    try:
        data.to_sql('geonames', con=db, index=False, if_exists='replace', chunksize=10000)
        print('Geonames main table loaded successfully.')
    except Exception as e:
        print('Load Failed: {}'.format(e))
    
    with db.connect() as con:
        try:
            con.execute('CREATE INDEX population_desc_index ON geonames (population DESC NULLS LAST);')
        except Exception as e:
            print('Indexing Failed: {}'.format(e))

    try:
        country_info.to_sql('country_info', con=db, index=False, if_exists='replace', chunksize=10000)
        print('Country info loaded successfully.')
    except Exception as e:
        print('Load Failed: {}'.format(e))


def main():     
        if not dbCheck():
            downloadData()
            data, country_info = transformData()
            loadData(data, country_info)
 
if __name__ == "__main__":
    main()